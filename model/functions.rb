
def createAccount
 request.body.rewind
  payload=JSON.parse(request.body.read)

  _username = payload["username"]
  _first_name = payload["first_name"]
  _last_name = payload["last_name"]
  _other_names = payload["other_names"]
  _email = payload["email"]
  _password = payload["password"]
  

	if User.where(:username => _username,:email => _email).exists?
	    halt Oj.dump(ERR_EXIST_ACC)
	elsif User.where(:username => _username).exists?
	    halt Oj.dump(ERR_EXIST_ACC)
	    	
	else
	  puts insertUser(_username,_first_name,_last_name,_other_names,_password,_email).inspect
		puts "-----------------------------------------------------------------------"
		puts "-----------------------------------------------------------------------"
		puts "-------------------------Sign Up Successful----------------------------"
		puts "-----------------------------------------------------------------------"
		puts "-----------------------------------------------------------------------"
	 # puts
	 	Oj.dump(STR_SIGN_UP_PASS)
	 	#puts
	end
end


def newContact
	request.body.rewind
  	payload=JSON.parse(request.body.read)

  	_name = payload["name"]
  	_number = payload["number"]
  	_location = payload["location"]
  	_user_id = payload["user_id"]

  	if Contact.where(:c_name =>_name,:number=>_number,:location=>_location).exists?
  		_res={
  			"resp_desc"=>"Failed",
  			"resp_code"=>"010",
  			"message"=>"Contact exists"
  		}
  		 puts _res.inspect
  		 halt Oj.dump(_res)
  		
  	else
  		puts insertContact(_name,_number,_location,_user_id)
  		_resp={
  			"resp_desc"=>"Successful",
  			"resp_code"=>"000",
  			"message"=>"Contact saved successfully"
  		}
  		puts _resp.inspect 
  		Oj.dump(_resp)
  		
  	end
end

def loginAccount
	request.body.rewind
  	payload=JSON.parse(request.body.read)

  	_username = payload["username"]
  	_password = payload["password"]

  	if User.exists?(:username => _username,:password =>_password)
		#Oj.dump(LOGIN_OBJECT)
		puts user=User.where(username: _username).select("username,firstname,othernames,lastname")[0]
		
		_hash={
			"user_name"=>"#{user.username}",
			"first_name"=>"#{user.firstname}",
			"last_name"=>"#{user.lastname}"
		}

		puts _res = {"resp_desc" => "Success", "resp_code" => "000", "message" => "Login Successful", "results"=>_hash}
		
		puts "-----------------------------------------------------------------------"
		puts "-----------------------------------------------------------------------"
		puts "-------------------------Sign In Successful----------------------------"
		puts "-----------------------------------------------------------------------"
		puts "-----------------------------------------------------------------------"

		Oj.dump(_res)

	else
		halt Oj.dump(ERR_SIGN_IN_FAIL)
	end
end

def editContact
	request.body.rewind
  	payload=JSON.parse(request.body.read)

  	_id = payload["id"]
  
      value = Contact.where(:id=> _id).order('c_name asc')
  	if value.exists?
		
		contact=value[0]
      
		 puts _info={"id" => _id,"name" => "#{contact.c_name}","number" => "#{contact.number}","location" => "#{contact.location}","status" => "#{contact.status}"}

		Oj.dump(_info)
	else
		puts _failedInfo={"resp_desc" => "Not found","resp_code" =>"111","message"=>"Contact not found"}

		Oj.dump(_failedInfo)
	end
end

def showContact
	request.body.rewind
  	payload=JSON.parse(request.body.read)

  	_user = payload["user_id"]
  	_stat = payload["status"]
  
      enabled = Contact.where(:user_id=> _user,:status => _stat).order('c_name asc')
      disabled = Contact.where(:user_id=> _user,:status => _stat).order('c_name asc')
  	if enabled.exists?
		
		contact=enabled[0]
      
		 #puts _info={"id" => "#{contact.id}","name" => "#{contact.c_name}","number" => "#{contact.number}","location" => "#{contact.location}","status" => "#{contact.status}"}
		 puts contact
		MultiJson.dump(enabled,:pretty => true)

	elsif disabled.exists?
		
		contact=disabled[0]
      
		 puts _info={"id" => _id,"name" => "#{contact.c_name}","number" => "#{contact.number}","location" => "#{contact.location}","status" => "#{contact.status}"}		
		puts _failedInfo={"resp_desc" => "Deleted","resp_code" =>"111","message"=>"Contact is deleted"}

		MultiJson.dump(disabled,:pretty => true)
	end
end

def viewContact
	request.body.rewind
	payload=JSON.parse(request.body.read)

	_id=payload["id"]
	_user=payload["user_id"]

	selected=Contact.where(:id => _id,:user_id => _user)

	if selected.exists?
		secContact=selected[0]
		puts _info={"id" => _id,"name" => "#{secContact.c_name}","number" => "#{secContact.number}","location" => "#{secContact.location}","status" => "#{secContact.status}","user_id" => _user}
		Oj.dump(_info)
	else

	end
end

def updateContact
    request.body.rewind
  	payload=JSON.parse(request.body.read)

  	_id = payload["id"]
  	_name = payload["name"]
  	_number = payload["number"]
  	_location = payload["location"]
      
    value = Contact.where(id: _id).order('c_name asc')


  	if value.exists?
		#puts user=Contact.where(id: _id).select("id,c_name,number,location")[0].inspect
        con_update = value.update_all(:c_name => _name, :number => _number, :location => _location,:updated_at => DateTime.now)
        puts _Info={"resp_desc" => "Updated","resp_code" =>"000","message"=>"Contact successfully updated"}

        Oj.dump(_Info)
    else
    	 puts _InfoF={"resp_desc" => "Updated","resp_code" =>"111","message"=>"Contact updating failed"}

        Oj.dump(_InfoF)
    end
end


def deleteContact
    request.body.rewind
  	payload=JSON.parse(request.body.read)

  	_id = payload["id"]
  	
      value = Contact.where(id: _id).order('c_name asc')
  	if value.exists?
        con_delete = value.update_all(:status => false)
        puts _Info={"resp_desc" => "Deleted","resp_code" =>"000","message"=>"Contact successfully deleted"}

        Oj.dump(_Info)
    else
    	puts _InfoF={"resp_desc" => "Failure","resp_code" =>"111","message"=>"Contact failed to delete"}

        Oj.dump(_InfoF)
    end
end


def insertUser(usernme,first_nme,last_nme,other_nme,pass,email)
	puts User.create!(firstname:  first_nme,username:usernme,lastname:last_nme,othernames:other_nme,password:pass,email: email)
end

def insertContact(c_name,number,location,user_id)
	Contact.create(c_name: c_name,number: number,location: location,status: true,user_id: user_id,created_at: DateTime.now)
end

