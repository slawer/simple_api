require 'sinatra'
require 'active_record'
require 'json'
require 'net/http'
require 'yaml'
require 'savon'
require 'faraday'
require 'digest/md5'
require 'openssl'
require 'nokogiri'
require 'date'
require 'oj'
require 'multi_json'
require './constants'
require './model/functions'
require './model/db_config'

set :public_folder, File.dirname(__FILE__)

#set :bind, '192.168.43.115'
set :bind,'192.168.10.38'
#set :bind, '192.168.137.1'
set :port, '9090'


post "/sign_up" do
	createAccount
end

post "/sign_in" do
	loginAccount
end

post "/new_contact" do
	newContact
end

post "/edit_contact" do
	editContact
end

post "/update_contact" do
	updateContact
end

post "/delete_contact" do
	deleteContact
end

post "/show_contact" do
	showContact
end

post "/view_contact" do
	viewContact
end

post "/save_image" do
	@filename=params[:file][:filename]
	file=params[:file][:tempfile]
	File.open("./public/#{@filename}", "wb") do |f|  
		f.write(file.read)
	end
end
