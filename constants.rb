STR_SIGN_UP_PASS = {"resp_desc" => "Success", "resp_code" => "000", "message" => "Successfully signed up."}
STR_SIGN_IN_PASS = { "resp_desc" => "Success", "resp_code" => "000", "message" => "Login Successful"}
ERR_SIGN_UP_FAIL = {"resp_desc" => "Error", "resp_code" => "101", "message" => "Sorry something went wrong. Please try again."}
ERR_SIGN_IN_FAIL = {"resp_desc" => "Error", "resp_code" => "102", "message" => "Please provide valid details"}
ERR_EXIST_ACC = {"resp_desc" => "Error", "resp_code" => "106", "message" => "User account exists!"}
ERR_SYS_ACC = {"resp_desc" => "Error", "resp_code" => "106", "message" => "User account doesn't exists!"}

